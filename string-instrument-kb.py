from mingus.midi import fluidsynth
from mingus.containers import Note
import tkinter as tk
fluidsynth.init("soundfont.sf2")
def key_pressed(event):
    if event.char not in active_keys:
        if event.char in strings:
            for string in strings:
                if string in active_keys:
                    active_keys.remove(string)
        active_keys.append(event.char)
        
    
def key_released(event):
    if event.char not in strings:
        active_keys.remove(event.char)
active_keys = [""]
active_notes = []
window = tk.Tk()
strings = ["a","s","d","f"]
# Prioritize higher fingers over lower ones (pressing first and second finger results in playing second note)
fingers = ["j","k","l",""]
sharp_key = "'"
flat_key = ";"
notes = {
    strings[0]+fingers[3]:"C-4", strings[0]+fingers[2]:"D-4", strings[0]+fingers[1]:"E-4", strings[0]+fingers[0]:"F-4",
    strings[1]+fingers[3]:"G-4", strings[1]+fingers[2]:"A-4", strings[1]+fingers[1]:"B-4", strings[1]+fingers[0]:"C-5",
    strings[2]+fingers[3]:"D-5", strings[2]+fingers[2]:"E-5", strings[2]+fingers[1]:"F-5", strings[2]+fingers[0]:"G-5",
    strings[3]+fingers[3]:"A-5", strings[3]+fingers[2]:"B-5", strings[3]+fingers[1]:"C-6", strings[3]+fingers[0]:"D-6"
}
def main(strings, fingers, sharp_key, flat_key, notes):
    if " " not in active_keys:
        for note in active_notes:
                fluidsynth.stop_Note(Note(note))
                active_notes.remove(note)
        window.after(0, main, strings, fingers, sharp_key, flat_key, notes)
        return
    for string_key in strings:
        if string_key not in active_keys:
            continue
        for finger_key in fingers:
            if finger_key not in active_keys:
                continue
            new_note = notes[string_key+finger_key]
            
            if sharp_key in active_keys and flat_key not in active_keys:
                new_note = new_note[0] + "#" + new_note[1:]
            elif flat_key in active_keys and sharp_key not in active_keys:
                new_note = new_note[0] + "b" + new_note[1:]
            if new_note in active_notes:
                break
            for note in active_notes:
                if note != new_note:
                    fluidsynth.stop_Note(Note(note))
            active_notes.clear()
            active_notes.append(new_note)
            fluidsynth.play_Note(Note(new_note))
            break
        else:
            continue
        break
    else:
        if active_notes != []:
            for note in active_notes:
                fluidsynth.stop_Note(Note(note))
                active_notes.remove(note)
    window.after(0, main, strings, fingers, sharp_key, flat_key, notes)

for r in range(2):
   for c in range(11):
      tk.Label(window, text=[["a","r","s","t"," ","n","e","i"," ","o","'"],["C String", "G String", "D String", "A String", "", "First Finger", "Second Finger", "Third Finger","","Flat","Sharp"]][r][c],borderwidth=1 ).grid(row=r,column=c)

window.bind("<Key>", key_pressed)
window.bind("<KeyRelease>", key_released)
window.after(0, main, strings, fingers, sharp_key, flat_key, notes)
window.mainloop()