# string-instrument-kb

A "piano typing" alternative for people who play string instruments.

## Usage

First, you need to install a sound font. Place it in the folder and name it `soundfont.sf2`

Then run the program with `python3 string-instrument-kb.py`

Try pressing `A` and `J`, then pressing `Space` to play